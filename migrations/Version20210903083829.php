<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210903083829 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE movie_todo ADD movies_id INT NOT NULL');
        $this->addSql('ALTER TABLE movie_todo ADD CONSTRAINT FK_A96897AD53F590A4 FOREIGN KEY (movies_id) REFERENCES movie (id)');
        $this->addSql('CREATE INDEX IDX_A96897AD53F590A4 ON movie_todo (movies_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE movie_todo DROP FOREIGN KEY FK_A96897AD53F590A4');
        $this->addSql('DROP INDEX IDX_A96897AD53F590A4 ON movie_todo');
        $this->addSql('ALTER TABLE movie_todo DROP movies_id');
    }
}
