<?php

namespace App\Controller\Movie;


use App\Form\MovieSearchType;
use App\Services\ImdbApi;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class MoviesController extends AbstractController
{
    private ImdbApi $imdbApi;

    public function __construct(ImdbApi $imdbApi)
    {
        $this->imdbApi = $imdbApi;
    }




    /**
     * @Route("/movies/search", name="movies_search")
     */
    public function searchMovies(Request $request): Response
    {
        $data = null;
        $error  = "";

        $form = $this->createForm(MovieSearchType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $searchParams = $form->getData();

            try {
                $data = $this->imdbApi->searchData($searchParams);
            } catch (\Exception $e) {
                $error = $e->getCode();
            }
        }
        return $this->render('movies/search.html.twig', [
            'user'  => $this->getUser(),
            'form'  => $form->createView(),
            'data'  => $data,
            'error' => $error
        ]);
    }





    /**
     * @Route("/movies/show/{imdbId}", name="movie_show")
     */
    public function showMovie($imdbId): Response
    {
        $error     = "";
        $movieData = $this->imdbApi->getMovieDetails($imdbId);

        return $this->render('movies/showMovie.html.twig', [
            'showType'  => "BASE",
            'movieData' => $movieData,
            'error'     => $error
        ]);
    }

}
