<?php

namespace App\Controller\Movie;

use App\Entity\MovieTodo;
use App\Repository\MovieTodoRepository;
use App\Services\ImdbApi;
use App\Services\MovieHelper;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MoviesTodoController extends AbstractController

{
    private ImdbApi $imdbApi;
    private MovieHelper $movieHelper;

    public function __construct( MovieHelper $movieHelper, ImdbApi $imdbApi)
    {
        $this->movieHelper = $movieHelper;
        $this->imdbApi     = $imdbApi;
    }

    /**
     * @Route("/user/watchlist", name="user_watchlist")
     */
    public function watchlist(MovieTodoRepository $movieTodo): Response
    {
        $user            = $this->getUser();
        $watchListMovies = $movieTodo->findBy(['user' => $user]);
        $watchListData   = [];

        foreach ($watchListMovies as $movie) {

            $movieDetails = $this->imdbApi->getMovieDetails($movie->getMovie()->getImdbId());

            $watchListData[] = [
                "movieTodo" => $movie,
                "movieData" => $movieDetails
            ];
        }

        return $this->render('movies/toDoMovie_list.html.twig', [
            'watchList' => $watchListData,
            'user' =>$this->getUser()
        ]);
    }


    /**
     * @Route("/movies/movieTodo/{imdbId}", name="add_movie_todo")
     */
    public function addMovieTodo($imdbId): RedirectResponse
    {
        $manager = $this->getDoctrine()->getManager();

        $user  = $this->getUser();
        $movie = $this->movieHelper->generateMovie($imdbId);

        $movieTodo = new MovieTodo();
        $movieTodo->setCreatedAt();
        $movieTodo->setUser($user);
        $movieTodo->setMovie($movie);

        $manager->persist($movie);
        $manager->persist($movieTodo);
        $manager->flush();

        return $this->redirectToRoute('movies_search');
    }

    /**
     * @Route("/movies/movieTodo/delete/{id}", name="delete_movie_todo")
     */
    public function deleteMovieTodo(MovieTodo $movieTodo): Response
    {
        if ($this->getUser() === $movieTodo->getUser()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($movieTodo);
            $entityManager->flush();
        }

        return $this->redirectToRoute('user_watchlist');
    }

    /**
     * @Route("/movies/movieTodo/show/{id}", name="show_movie_todo")
     */
    public function showMovieTodo(MovieTodo $movieTodo): Response
    {
        $movieData = $this->imdbApi->getMovieDetails($movieTodo->getMovie()->getImdbId());

        return $this->render('movies/showMovie.html.twig', [
            'movieData' => $movieData,
            'movieTodo' => $movieTodo,
            'showType' => 'TODO'
            ]);
    }
}
