<?php

namespace App\Controller\Movie;

use App\Entity\Comment;
use App\Entity\MovieWatched;
use App\Form\CommentType;
use App\Repository\MovieWatchedRepository;
use App\Services\ImdbApi;
use App\Services\MovieHelper;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MoviesWatchedController extends AbstractController
{
    private ImdbApi $imdbApi;
    private MovieHelper $movieHelper;

    public function __construct(ImdbApi $imdbApi, MovieHelper $movieHelper)
    {
        $this->imdbApi     = $imdbApi;
        $this->movieHelper = $movieHelper;
    }


    /**
     * @Route("/user/watchedMovies", name="user_watched_movies")
     */
    public function watchedMoviesList(MovieWatchedRepository $movieWatchedRepo) : Response
    {
        $user                 = $this->getUser();
        $moviesWatchedDisplay = $this->movieHelper
            ->getWatchedMoviesData($movieWatchedRepo, $user);

        return $this->render('movies/watchedMovie_list.html.twig', [
            'watchList' => $moviesWatchedDisplay,
        ]);
    }

    /**
     * @Route("/movies/watchedMovie/add/{imdbId}", name="add_watched_movie")
     */
    public function addWatchedMovie(Request $request, $imdbId): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $movieDetails  = $this->imdbApi->getMovieDetails($imdbId);
        $user          = $this->getUser();
        $movie         = $this->movieHelper->generateMovie($imdbId);
        $watched       = new MovieWatched();
        $comment       = new Comment();

        $entityManager->persist($movie);

        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            $comment
                ->setCreatedAt()
                ->setUser($user)
                ->setMovieWatched($watched);
            $entityManager->persist($comment);

            $watched
                ->setUser($user)
                ->setCreatedAt()
                ->setMovie($movie)
                ->setComment($comment);

            $entityManager->persist($watched);
            $entityManager->flush();

            return $this->redirectToRoute('user_watched_movies');

        }

        return $this->render('movies/comment/new_comment.html.twig', [
            'movieDetails' => $movieDetails,
            'form'         => $form->createView(),
        ]);
    }

    /**
     * @Route("/movies/watchedMovie/delete/{id}", name="delete_watched_movie")
     */
    public function deleteWatchedMovie(MovieWatched $movieWatched): Response
    {
        if ($this->getUser() === $movieWatched->getUser()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($movieWatched);
            $entityManager->flush();
        }
        return $this->redirectToRoute('user_watched_movies');
    }

    /**
     * @Route("/movies/watchedMovie/update/{id}", name="update_watched_movie")
     */
    public function updateWatchedMovie(MovieWatched $movieWatched, Request $request)
    {
        $comment = $movieWatched->getComment();
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $comment->setLastUpdatedAt();


            $entityManager->flush();

            return $this->redirectToRoute('user_watched_movies');
        }
        return $this->render('movies/comment/edit_comment.html.twig', [
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/movies/watchedMovie/show/{id}", name="show_watched_movie")
     */
    public function showWatchedMovie(MovieWatched $movieWatched) : Response
    {
        $movieData = $this->imdbApi->getMovieDetails($movieWatched->getMovie()->getImdbId());



        return $this->render('movies/showMovie.html.twig', [
            'movieData' => $movieData,
            'movieWatched' => $movieWatched,
            'showType' => 'WATCHED',
            'isEditAuthorized' => ($movieWatched->getUser() === $this->getUser()) ? true : false,

        ]);
    }
}
