<?php

namespace App\Controller\Club;

use App\Entity\Club;
use App\Entity\MovieTodo;
use App\Entity\MovieWatched;
use App\Entity\User;
use App\Entity\UserClub;
use App\Form\ClubType;
use App\Form\ClubUsersType;
use App\Form\UserClubType;
use App\Repository\UserClubRepository;
use App\Services\MovieHelper;
use phpDocumentor\Reflection\Types\Boolean;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ClubController extends AbstractController
{

    private MovieHelper $movieHelper;
    private UserClubRepository $clubs;

    public function __construct(MovieHelper $movieHelper, UserClubRepository $userClubRepository)
    {
        $this->movieHelper = $movieHelper;
        $this->clubs       = $userClubRepository;
    }

    /**
     * @Route("/club/", name="club")
     */
    public function index(): Response
    {
        $user      = $this->getUser();
        $userClubs = $this->clubs->findBy(["user" => $user]);

        $userClubsData = [];
        $waitingClubs  = [];

        foreach ($userClubs as $userClub) {
            if ($userClub->getIsActive() === true) {
                $lastUpdatedDate = $userClub->getUpdatedAt();
                if ($lastUpdatedDate === null) {
                    $lastUpdatedDate = $userClub->getCreatedAt();
                }

                $users    = $this->getUsersFromClub($this->clubs, $userClub);
                $clubData = [
                    "idClub"          => $userClub->getClub()->getId(),
                    "idUserClub"      => $userClub->getId(),
                    "name"            => $userClub->getClub()->getName(),
                    "user_role"       => $userClub->getUserRole(),
                    "lastUpdatedDate" => $lastUpdatedDate,
                    "users"           => $users
                ];

                $userClubsData[] = $clubData;
            } else {
                $waitingClubs[] = [
                    "idUserClub"   => $userClub->getId(),
                    "nameUserClub" => $userClub->getClub()->getName()
                ];
            }
        }
        return $this->render('club/club_list.html.twig', [
            'userClubs'     => $userClubsData,
            "userConnected" => $user,
            "waitingClubs"  => $waitingClubs

        ]);
    }

    /**
     * @Route("/club/show/user/{id}/watched", name="show_club_user_watchedlist")
     */
    public function showUserWatchedMovieList(User $user)
    {

        if ($this->isUserInSameGroup($user, $this->getUser())) {

            $movieWatchedRepo     = $this->getDoctrine()
                ->getRepository(MovieWatched::class);
            $moviesWatchedDisplay = $this->movieHelper
                ->getWatchedMoviesData($movieWatchedRepo, $user);

            return $this->render('club/list_watched.html.twig', [
                "user"          => $user,
                'moviesWatched' => $moviesWatchedDisplay,

            ]);

        } else {
            throw $this->createNotFoundException("Vous n'avez pas accès à cette page !");
        }
    }

    /**
     * @Route("/club/show/user/{id}/watchlist", name="show_club_user_Todolist")
     */
    public function showUserTodoMovieList(User $user)
    {
        if ($this->isUserInSameGroup($user, $this->getUser())) {

            $movieTodoRepo     = $this->getDoctrine()
                ->getRepository(MovieTodo::class);
            $moviesTodoDisplay = $this->movieHelper
                ->getMoviesTodoData($movieTodoRepo, $user);

            return $this->render('club/show_watchlist.html.twig', [
                "user"              => $user,
                'moviesTodoDisplay' => $moviesTodoDisplay,
            ]);
        } else {
            throw $this->createNotFoundException("Vous n'avez pas accès à cette page !");
        }
    }


    /**
     * @Route("/club/create", name="club_creation")
     */
    public function createClub(Request $request)
    {
        $club = new Club();
        $form = $this->createForm(ClubType::class, $club);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();

            $club->setCreatedAt();
            $entityManager->persist($club);

            $userClub = new UserClub();
            $userClub
                ->setCreatedAt()
                ->setUserRole('ADMIN')
                ->setIsActive(true)
                ->setUser($this->getUser())
                ->setClub($club);

            $entityManager->persist($userClub);
            $entityManager->flush();

            return $this->redirectToRoute('club');
        }

        return $this->render('club/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/club/{id}/addUser", name="add_user")
     */
    public function addUserInClub(Club $club, Request $request)
    {
        $userClub = new UserClub();
        $form     = $this->createForm(UserClubType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() &&
            $form->isValid() &&
            $this->isUserMailExists($form->getData()["email"])) {

            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->persist($userClub);
            $userClub
                ->setCreatedAt()
                ->setUserRole('USER')
                ->setIsActive(false)
                ->setUser($this->getUserByMail($form->getData()["email"]))
                ->setClub($club);

            $entityManager->persist($userClub);
            $entityManager->flush();

            return $this->redirectToRoute('club');
        }

        return $this->render('club/add_user.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/club/delete/{id}", name="delete_club_user")
     */
    public function deleteClubUser(UserClub $userClub): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($userClub);
        $entityManager->flush();

        return $this->redirectToRoute('club');
    }

    /**
     * @Route ("/club/accept/{id}", name="accept_club")
     */
    public function acceptClub(UserClub $userClub): Response
    {
        $userClub->setIsActive(true);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($userClub);
        $entityManager->flush();
        return $this->redirectToRoute('club');
    }



    private function getUsersFromClub($clubs, $userClub): array
    {
        $users = [];

        foreach ($clubs->findBy(["club" => $userClub->getClub()]) as $usersInClub) {
            if ($usersInClub->getIsActive() === true) {
                $users[] = [
                    "user" => $usersInClub->getUser(),
                    "role" => $usersInClub->getUserRole()
                ];
            }
        }
        return $users;
    }


    private function isUserInSameGroup($targetUser, $connectedUser): bool
    {
        $isAuthorized   = false;
        $userClubs      = $this->clubs->findBy(["user" => $targetUser]);
        $otherUserClubs = $this->clubs->findBy(["user" => $connectedUser]);
        $clubsUsers     = [];

        foreach ($otherUserClubs as $otherUserClub) {
            $clubsUsers[] = $this->clubs->findBy(["club" => $otherUserClub->getClub()]);
        }

        foreach ($clubsUsers as $clubUsers) {
            foreach ($clubUsers as $otherUser) {
                if ($otherUser->getUser() === $targetUser) {
                    $isAuthorized = true;
                }
            }
        }
        return $isAuthorized;
    }

    private function isUserMailExists(string $email): bool
    {
        $user = $this->getUserByMail($email);

        if (empty($user)) {
            $this->addFlash("maildontexist", "Cet Utilisateur n'existe pas ! N'hésitez pas à parler de Movies Recall à vos amis ! Ou alors, c'est une faute de frappe !");
            return false;
        } else {
            return true;
        }
    }

    private function getUserByMail(string $email)
    {
        $userRepository = $this->getDoctrine()->getRepository(User::class);
        return $userRepository->findOneBy(["email" => $email]);
    }

}
