<?php

namespace App\Repository;

use App\Entity\MovieTodo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MovieTodo|null find($id, $lockMode = null, $lockVersion = null)
 * @method MovieTodo|null findOneBy(array $criteria, array $orderBy = null)
 * @method MovieTodo[]    findAll()
 * @method MovieTodo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MovieTodoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MovieTodo::class);
    }

    // /**
    //  * @return MovieTodo[] Returns an array of MovieTodo objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MovieTodo
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
