<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
 */
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Assert\NotBlank(
     *     message="Votre email ne doit pas être vide !"
     * )
     * @Assert\Email(
     *     message="Votre email n'est pas valide !"
     * )
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     * @Assert\Length(
     *     min = 8,
     *     minMessage="Votre mot de passe doit faire au moins {{ limit }} caractères"
     * )
     */
    private $password;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $created_at;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(
     *     message="Vous devez choisir un pseudonyme !"
     * )
     */
    private $username;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isVerified = false;

    /**
     * @ORM\OneToMany(targetEntity=MovieTodo::class, mappedBy="user", orphanRemoval=true)
     */
    private $movieTodos;

    /**
     * @ORM\OneToMany(targetEntity=MovieWatched::class, mappedBy="User")
     */
    private $moviesWatched;

    /**
     * @ORM\OneToMany(targetEntity=Comment::class, mappedBy="User")
     */
    private $comments;

    /**
     * @ORM\OneToMany(targetEntity=UserClub::class, mappedBy="user")
     */
    private $user_club;

    public function __construct()
    {
        $this->movieTodos = new ArrayCollection();
        $this->moviesWatched = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->user_club = new ArrayCollection();
    }
    public function __toString() {
        return $this->email;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @deprecated since Symfony 5.3, use getUserIdentifier instead
     */
    public function getUsername(): string
    {
        return (string) $this->username;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeImmutable $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function isVerified(): bool
    {
        return $this->isVerified;
    }

    public function setIsVerified(bool $isVerified): self
    {
        $this->isVerified = $isVerified;

        return $this;
    }

    /**
     * @return Collection|MovieTodo[]
     */
    public function getMovieTodos(): Collection
    {
        return $this->movieTodos;
    }

    public function addMovieTodo(MovieTodo $movieTodo): self
    {
        if (!$this->movieTodos->contains($movieTodo)) {
            $this->movieTodos[] = $movieTodo;
            $movieTodo->setUser($this);
        }

        return $this;
    }

    public function removeMovieTodo(MovieTodo $movieTodo): self
    {
        if ($this->movieTodos->removeElement($movieTodo)) {
            // set the owning side to null (unless already changed)
            if ($movieTodo->getUser() === $this) {
                $movieTodo->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|MovieWatched[]
     */
    public function getMoviesWatched(): Collection
    {
        return $this->moviesWatched;
    }

    public function addMoviesWatched(MovieWatched $moviesWatched): self
    {
        if (!$this->moviesWatched->contains($moviesWatched)) {
            $this->moviesWatched[] = $moviesWatched;
            $moviesWatched->setUser($this);
        }

        return $this;
    }

    public function removeMoviesWatched(MovieWatched $moviesWatched): self
    {
        if ($this->moviesWatched->removeElement($moviesWatched)) {
            // set the owning side to null (unless already changed)
            if ($moviesWatched->getUser() === $this) {
                $moviesWatched->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setUser($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->removeElement($comment)) {
            // set the owning side to null (unless already changed)
            if ($comment->getUser() === $this) {
                $comment->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserClub[]
     */
    public function getUserClub(): Collection
    {
        return $this->user_club;
    }

    public function addUserClub(UserClub $userClub): self
    {
        if (!$this->user_club->contains($userClub)) {
            $this->user_club[] = $userClub;
            $userClub->setUser($this);
        }

        return $this;
    }

    public function removeUserClub(UserClub $userClub): self
    {
        if ($this->user_club->removeElement($userClub)) {
            // set the owning side to null (unless already changed)
            if ($userClub->getUser() === $this) {
                $userClub->setUser(null);
            }
        }

        return $this;
    }
}
