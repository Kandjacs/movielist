<?php

namespace App\Entity;

use App\Repository\MovieRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MovieRepository::class)
 */
class Movie
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $imdbId;

    /**
     * @ORM\OneToMany(targetEntity=MovieTodo::class, mappedBy="Movies")
     */
    private $movieTodos;

    /**
     * @ORM\OneToMany(targetEntity=MovieWatched::class, mappedBy="Movie")
     */
    private $moviesWatched;

    public function __construct()
    {
        $this->movieTodos = new ArrayCollection();
        $this->moviesWatched = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getImdbId(): ?string
    {
        return $this->imdbId;
    }

    public function setImdbId(string $imdbId): self
    {
        $this->imdbId = $imdbId;

        return $this;
    }

    /**
     * @return Collection|MovieTodo[]
     */
    public function getMovieTodos(): Collection
    {
        return $this->movieTodos;
    }

    public function addMovieTodo(MovieTodo $movieTodo): self
    {
        if (!$this->movieTodos->contains($movieTodo)) {
            $this->movieTodos[] = $movieTodo;
            $movieTodo->setMovies($this);
        }

        return $this;
    }

    public function removeMovieTodo(MovieTodo $movieTodo): self
    {
        if ($this->movieTodos->removeElement($movieTodo)) {
            // set the owning side to null (unless already changed)
            if ($movieTodo->getMovies() === $this) {
                $movieTodo->setMovies(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|MovieWatched[]
     */
    public function getMoviesWatched(): Collection
    {
        return $this->moviesWatched;
    }

    public function addMoviesWatched(MovieWatched $moviesWatched): self
    {
        if (!$this->moviesWatched->contains($moviesWatched)) {
            $this->moviesWatched[] = $moviesWatched;
            $moviesWatched->setMovie($this);
        }

        return $this;
    }

    public function removeMoviesWatched(MovieWatched $moviesWatched): self
    {
        if ($this->moviesWatched->removeElement($moviesWatched)) {
            // set the owning side to null (unless already changed)
            if ($moviesWatched->getMovie() === $this) {
                $moviesWatched->setMovie(null);
            }
        }

        return $this;
    }
}
