<?php

namespace App\Entity;

use App\Repository\ClubRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ClubRepository::class)
 */
class Club
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $created_at;

    /**
     * @ORM\OneToMany(targetEntity=UserClub::class, mappedBy="club")
     */
    private $user_club;

    public function __construct()
    {
        $this->user_club = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->created_at;
    }

    public function setCreatedAt(): self
    {
        $dateNow = new DateTimeImmutable();

        $this->created_at = $dateNow;


        return $this;
    }

    /**
     * @return Collection|UserClub[]
     */
    public function getUserClub(): Collection
    {
        return $this->user_club;
    }

    public function addUserClub(UserClub $userClub): self
    {
        if (!$this->user_club->contains($userClub)) {
            $this->user_club[] = $userClub;
            $userClub->setClub($this);
        }

        return $this;
    }

    public function removeUserClub(UserClub $userClub): self
    {
        if ($this->user_club->removeElement($userClub)) {
            // set the owning side to null (unless already changed)
            if ($userClub->getClub() === $this) {
                $userClub->setClub(null);
            }
        }

        return $this;
    }
}
