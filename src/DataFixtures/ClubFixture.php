<?php

namespace App\DataFixtures;

use App\Entity\Club;
use App\Entity\MovieWatched;
use App\Entity\User;
use App\Entity\UserClub;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ClubFixture extends Fixture implements DependentFixtureInterface
{
    public function getDependencies(): array
    {
        return array(
            UserFixtures::class,

        );
    }

    public function load(ObjectManager $manager)
    {

        $users = [
            $this->getReference("user1"),
            $this->getReference("user2"),
            $this->getReference("user3"),
            $this->getReference("user4"),
            $this->getReference("user5"),
        ];

        $club1 = new Club();
        $club1
            ->setName("club1")
            ->setCreatedAt();
        $manager->persist($club1);

        $userGroups1 = [
            $this->createUserClub($users[0], 'ADMIN', true, $club1),
            $this->createUserClub($users[1], 'USER', true, $club1),
            $this->createUserClub($users[2], 'USER', true, $club1),
            $this->createUserClub($users[3], 'USER', false, $club1),
            $this->createUserClub($users[4], 'USER', false, $club1),
        ];
        foreach ($userGroups1 as $ug) {
            $manager->persist($ug);
        }

        $club2 = new Club();
        $club2
            ->setName("club2")
            ->setCreatedAt();
        $manager->persist($club2);

        $userGroups2 = [
            $this->createUserClub($users[0], 'USER', true, $club2),
            $this->createUserClub($users[1], 'USER', true, $club2),
            $this->createUserClub($users[2], 'ADMIN', true, $club2),
            $this->createUserClub($users[3], 'USER', false, $club2),
            $this->createUserClub($users[4], 'USER', false, $club2),
        ];
        foreach ($userGroups2 as $ug) {
            $manager->persist($ug);
        }

        $manager->flush();
    }

    private function createUserClub(User $user, string $role, bool $active, Club $club): UserClub
    {
        $userClub = new UserClub();
        $userClub
            ->setCreatedAt()
            ->setIsActive($active)
            ->setUserRole($role)
            ->setUser($user)
            ->setClub($club);

        return $userClub;
    }
}
