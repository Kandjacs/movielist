<?php

namespace App\DataFixtures;

use App\Entity\MovieTodo;
use App\Entity\User;
use App\Repository\UserRepository;
use DateTimeImmutable;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class MovieTodoFixtures extends Fixture implements DependentFixtureInterface
{
    public function getDependencies(): array
    {
        return array(
            UserFixtures::class,
            MovieFixtures::class,

        );
    }

    public function load(ObjectManager $manager)
    {

        $users  = [
            "user1" => $this->getReference("user1"),
            "user2" => $this->getReference("user2"),
            "user3" => $this->getReference("user3"),
            "user4" => $this->getReference("user4"),
            "user5" => $this->getReference("user5"),
            "user6" => $this->getReference("user6"),
        ];
        $movies = [
            $this->getReference("movie1"),
            $this->getReference("movie2"),
            $this->getReference("movie3"),
            $this->getReference("movie4"),
            $this->getReference("movie5"),
            $this->getReference("movie6"),
            $this->getReference("movie7"),
            $this->getReference("movie8"),
            $this->getReference("movie9"),
            $this->getReference("movie10"),
            $this->getReference("movie11"),
            $this->getReference("movie12"),
            $this->getReference("movie13"),
            $this->getReference("movie14"),
        ];

        $this->createMovieTodo($users["user1"], $movies, 14, $manager);
        $this->createMovieTodo($users["user2"], $movies, 3,  $manager);
        $this->createMovieTodo($users["user3"], $movies, 7,  $manager);
        $this->createMovieTodo($users["user4"], $movies, 5,  $manager);
        $this->createMovieTodo($users["user5"], $movies, 10, $manager);
        $this->createMovieTodo($users["user6"], $movies, 9, $manager);

        $manager->flush();

    }

    private function createMovieTodo(User $user, array $movies, int $nbMovies, ObjectManager $manager): ObjectManager
    {
        for ($i = 0; $i < $nbMovies; $i++) {

            $movieTodo = new MovieTodo();
            $movieTodo->setUser($user);
            $movieTodo->setCreatedAt();
            $movieTodo->setMovie($movies[$i]);

            $manager->persist($movieTodo);
        }
        return $manager;
    }
}