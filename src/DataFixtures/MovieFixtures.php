<?php

namespace App\DataFixtures;

use App\Entity\Movie;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class MovieFixtures extends Fixture
{

    public function getDependencies(): array
    {
        return array(
            UserFixtures::class,
        );
    }
    public function load(ObjectManager $manager)
    {
        $moviesId = [
          ["tt0095016", "movie1"],
          ["tt0078748", "movie2"],
          ["tt0780504", "movie3"],
          ["tt0110912", "movie4"],
          ["tt0183790", "movie5"],
          ["tt0090728", "movie6"],
          ["tt0096256", "movie7"],
          ["tt0425112", "movie8"],
          ["tt3890160", "movie9"],
          ["tt0109440", "movie10"],
          ["tt0117998", "movie11"],
          ["tt0105236", "movie12"],
          ["tt0059578", "movie13"],
          ["tt0067355", "movie14"]
        ];

        foreach ($moviesId as $movieId)
        {
            $movie = new Movie();
            $movie->setImdbId($movieId[0]);

            $this->addReference($movieId[1], $movie);

            $manager->persist($movie);
        }


        $manager->flush();
    }
}
