<?php

namespace App\DataFixtures;

use App\Entity\User;
use DateTimeImmutable;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture
{
    private $passwordHasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }

    public function load(ObjectManager $manager)
    {

        $users = [
            "user1" => array("user1", "user1@mail.fr", "password"),
            "user2" => array("user2", "user2@mail.fr", "password"),
            "user3" => array("user3", "user3@mail.fr", "password"),
            "user4" => array("user4", "user4@mail.fr", "password"),
            "user5" => array("user5", "user5@mail.fr", "password"),
            "user6" => array("user6", "user6@mail.fr", "password"),
        ];

        foreach ($users as $userData) {
            $user = new User();
            $date = new DateTimeImmutable();

            $user->setUsername($userData[0]);
            $user->setEmail($userData[1]);
            $user->setCreatedAt($date);
            $user->setRoles(['ROLE_USER']);
            $user->setIsVerified(1);
            $user->setPassword($this->passwordHasher->hashPassword(
                $user,
                $userData[2]
            ));
            $this->addReference($userData[0], $user);
            $manager->persist($user);
        }

        $manager->flush();
    }
}
