<?php

namespace App\DataFixtures;

use App\Entity\Comment;
use App\Entity\Movie;
use App\Entity\MovieWatched;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class MoviesWatchedFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

        $watched1 = new MovieWatched();
        $user1 = $this->getReference("user1");
        $flashdance = new Movie;
        $flashdance ->setImdbId('tt0085549');
        $content1 = "What makes a good film for me is that it is enjoyable, entertaining, gripping, likable and emotional. Flashdance without a doubt ticks all these five boxes. Flahdance is a very retro, cheesy entertaining film that is at no point boring and never drags, and is also a very clear symbol of the great 1980's and the night club exotic dancing scene of the time. Jennifer Beals puts down an amazing performance, as a dancer and as an actress. I guess the story comes down to the fact that reality bites, that life is tough, but if you push hard you just might make it. All in all, Flashdance despite its few undeniable faults is a very decent and enjoyable film in my opinion. As it mostly meets all of my requirements I am happy to give it a positive review. This film is one of the quintessential 80's films, that works well as a symbolic artifact of its time. I highly recommend this film to anyone who loves 80's.";


        $manager->persist($flashdance);


        $watched1
            ->setCreatedAt()
            ->setUser($user1)
            ->setMovie($flashdance);
        $manager->persist($watched1);

        $comment1 = $this->createComment($user1, $watched1, $content1);
        $manager->persist($comment1);

        $watched1->setComment($comment1);

        $manager->persist($watched1);

        $manager->flush();
    }

    private function createComment(User $user, $movieWatched, $content) : Comment
    {
        $comment = new Comment();

        $comment
            ->setUser($user)
            ->setCreatedAt()
            ->setMovieWatched($movieWatched)
            ->setRating(7)
            ->setComment($content)
            ;


        return $comment;
    }
}
