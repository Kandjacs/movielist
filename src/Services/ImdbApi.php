<?php

namespace App\Services;


use Symfony\Contracts\HttpClient\HttpClientInterface;

class ImdbApi
{
    private HttpClientInterface $client;
    private string $apiKey;
    private string $apiLocale;


    public function __construct(string $apiKey, string $apiLocale, HttpClientInterface $client)
    {
        $this->client    = $client;
        $this->apiKey    = $apiKey;
        $this->apiLocale = $apiLocale;
    }


    public function getMovieDetails($id)
    {
        $api_URI = 'https://imdb-api.com/' . $this->apiLocale . '/API/Title/' . $this->apiKey . '/' . $id;

        try {
            $movieDetails = $this->client->request(
                'GET',
                $api_URI
            );
            if ($movieDetails !== null) {
                return json_decode($movieDetails->getContent());
            }

        } catch (\Exception $e) {
            error_log($e->getMessage());
            return null;
        }
    }




    public function searchData($searchParams)
    {
        $searchOption = $searchParams['searchOptions'];
        $searchValue  = $searchParams['searchValue'];
        $api_URI      = 'https://imdb-api.com/' . $this->apiLocale . '/API/Search' . $searchOption . '/' . $this->apiKey . '/' . $searchValue;

        $movieList = $this->client->request(
            'GET',
            $api_URI
        );
        return json_decode($movieList->getContent());
    }
}
