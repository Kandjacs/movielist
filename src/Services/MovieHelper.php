<?php

namespace App\Services;

use App\Entity\Movie;
use App\Entity\User;
use App\Repository\MovieTodoRepository;
use App\Repository\MovieWatchedRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class MovieHelper
{
    private EntityManagerInterface $em;
    private ImdbApi $imdbApi;

    public function __construct(EntityManagerInterface $em, ImdbApi $imdbApi)
    {
        $this->em      = $em;
        $this->imdbApi = $imdbApi;

    }

    /**
     * Creates a Movie if it doesn't exist in base,
     * or returns the already existing Movie if it does.
     *
     * @param $imdbId
     * @return Movie
     */

    public function generateMovie($imdbId): Movie
    {
        $existingMovie = $this->em
            ->getRepository(Movie::class)
            ->findOneBy([
                'imdbId' => $imdbId
            ]);

        if ($existingMovie === null) {
            $movie = new Movie();
            $movie->setImdbId($imdbId);
            $this->em->persist($movie);
            $this->em->flush();

        } else {
            $movie = $existingMovie;
        }

        return $movie;
    }

    public function getWatchedMoviesData(MovieWatchedRepository $movieWatchedRepo, UserInterface $user): array
    {
        $moviesWatchedData    = $movieWatchedRepo->findBy(['User' => $user]);
        $moviesWatchedDisplay = [];

        foreach ($moviesWatchedData as $movieData) {
            $movieComment    = $movieData->getComment()->getComment();
            $movieRating     = $movieData->getComment()->getRating();
            $movieDetails    = $this->imdbApi->getMovieDetails($movieData->getMovie()->getImdbId());
            $lastUpdatedDate = $movieData->getComment()->getLastUpdatedAt();
            if ($lastUpdatedDate === null) {
                $lastUpdatedDate = $movieData->getComment()->getCreatedAt();
            }

            $data = [
                "id"              => $movieData->getId(),
                "lastUpdatedDate" => $lastUpdatedDate,
                "comment"         => $movieComment,
                "rating"          => $movieRating,
                "details"         => $movieDetails
            ];

            $moviesWatchedDisplay[] = $data;
        }

        return $moviesWatchedDisplay;
    }

    public function getMoviesTodoData(MovieTodoRepository $movieTodoRepo, UserInterface $user) : array
    {
        $moviesTodo    = $movieTodoRepo->findBy(['user' => $user]);
        $moviesTodoDisplay = [];

        foreach ($moviesTodo as $movieTodo) {
            $moviesTodoDisplay[] = $this->imdbApi->getMovieDetails($movieTodo->getMovie()->getImdbId());
        }

        return $moviesTodoDisplay;
    }

}
