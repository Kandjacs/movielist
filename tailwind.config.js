const theme    = require('./assets/styles/tailwindConfig/theme')
const variants = require('./assets/styles/tailwindConfig/variants')
module.exports = {

  mode: 'jit',
  //---- UNCOMMENT ON DEV ----//
  purge: [
    './src/**/*.html',
    './templates/**/*.html.twig',
    './assets/js/**/*.js'
  ],
  safelist: [
    'bg-blue-500',
  ],
  darkMode: false, // or 'media' or 'class'

  theme,
  variants,

  plugins: [],
}
