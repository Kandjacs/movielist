module.exports = {
  extend: {

    fontFamily: {
      core: [
        'Electrolize'
      ],
      neon: [
        'Vibur',
      ],
    },

    colors: {
      transparent: 'transparent',
      background: '#101010',
      base: '#ddd',

      primary: {
        pink: '#F72585',
        blue: '#4CC9F0',
        magenta: '#3A0CA3'
      },
      secondary: {
        pink: '#7209B7',
        blue: '#4361EE'
      },
      neon: {
        pink: '#F72585',
        yellow: '#ff9100',
        red: '#f51b1b'
      }
    },

    screens: {
      'xxs': '250px',
      'xxs-down': { 'max': '249px' },
      'xs': '420px',
      'xs-down': { 'max': '419px' },
      'sm': '576px',
      'sm-down': { 'max': '575px' },
      'md': '768px',
      'md-down': { 'max': '767px' },
      'lg': '1025px',
      'lg-down': { 'max': '1024px' },
      'xl': '1240px',
      'xl-down': { 'max': '1239px' },
    },
    boxShadow: {
      pink: '0 1px 2px 0 rgba(247,37,133, 0.05)',
    }
  }

}